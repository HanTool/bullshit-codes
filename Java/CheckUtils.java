public class CheckUtils {

    private CheckUtils() {}

    // 其他方法略...

    /**
     * 验证实体
     *
     * @param obj   实体
     * @param group 实体组
     * @param <T>   实体类类型
     * @throws Exception /
     */
    public static <T> void checkEntity(T obj, Class<?>... group) throws Exception {
        if (obj == null) {
            throw new CheckException("参数不能为空");
        }
        Set<ConstraintViolation<T>> violations;
        Validator validator = SpringUtil.getBean(Validator.class);
        if (ArrayUtil.isEmpty(group)) {
            violations = validator.validate(obj);
        } else {
            violations = validator.validate(obj, group);
        }
        if (CollUtil.isNotEmpty(violations)) {
            // TODO for 循环里抛异常
            for (ConstraintViolation<T> constraintViolation : violations) {
                throw new CheckException(constraintViolation.getMessage());
            }
        }
    }
}